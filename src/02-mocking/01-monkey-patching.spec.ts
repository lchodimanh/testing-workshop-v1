/**
 * Mocking permite que nuestros tests sean determinísticos.
 *
 * La forma más naive de hacer mocking en Javascript es directamente
 * sobreescribir la propiedad en el objeto.
 *
 * En este caso vamos a hacer mocking de la función 'getWinner' para
 * que siempre retorne uno de los dos jugadores.
 *
 * Tarea: hazle un patch a `getWinner` para que siempre retorne un mismo
 * jugador en todas las llamadas.
 *
 * Ejecución: Use `jest src/02-mocking/01-monkey-patching` para ejecutar el test
 */

import { thumbWar } from './thumb-war';
import * as utils from './utils';

const p1 = 'Luis Chodiman';
const p2 = 'Juan Perez';

// Your code:
// monkey patch

test('monkey patching', () => {
  const winner = thumbWar(p1, p2);
  expect(winner).toBe(p1);

  // Your code:
  // cleanup
});
