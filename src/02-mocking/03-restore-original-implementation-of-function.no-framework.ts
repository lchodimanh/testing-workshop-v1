/**
 * Tarea: implementa a `spyOn`.
 *
 * Ejecución: Use `ts-node src/02-mocking/03-restore-original-implementation-of-function.no-framework.ts`
 */

import assert from 'assert';
import { thumbWar } from './thumb-war';
import * as utils from './utils';

function fn(impl = () => {}) {
  const mockFn = (...args: any[]) => {
    // @ts-ignore
    mockFn.mock.calls.push(args);
    // @ts-ignore
    return impl(...args);
  };
  mockFn.mock = { calls: [] };

  // Recuerda agregar una propiedad 'mockImplementation':

  return mockFn;
}

// Implementación de 'spyOn' acá:

spyOn(utils, 'getWinner');
// @ts-ignore
utils.getWinner.mockImplementation((p1, p2) => p1);

const winner = thumbWar('Luis Chodiman', 'Juan Perez');
assert.strictEqual(winner, 'Luis Chodiman');
// @ts-ignore
assert.deepStrictEqual(utils.getWinner.mock.calls, [
  ['Luis Chodiman', 'Juan Perez'],
  ['Luis Chodiman', 'Juan Perez'],
]);

// cleanup
// @ts-ignore
utils.getWinner.mockRestore();
