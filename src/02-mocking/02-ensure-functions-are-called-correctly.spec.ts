/**
 * De vez en cuando al escribir tests en Javascript
 * es necesario verificar que una función fue llamada
 * correctamente. Esto requiere que mantengamos un record
 * de cuán a menudo fue llamada la función y con qué argumentos.
 * De esta forma podemos hacer afirmaciones sobre cuántas veces y
 * con qué argumentos una función fue llamada.
 * Tarea: usa las assertions de Jest para verificar
 *      1. Cuán a menudo fue llamado la función
 *      2. Que la función haya sido llamada con los argumentos correctos
 *
 * Ejecuta: Use `npx jest --watch src/02-mocking/02-ensure-function-are-called-correclty.ts` para
 * correr el test en modo watch
 */

import { thumbWar } from './thumb-war';
import * as utils from './utils';

test('returns winner', () => {
  const originalGetWinner = utils.getWinner;
  // @ts-ignore
  utils.getWinner = jest.fn((p1, p2) => p1);

  const p1 = 'Luis Chodiman';
  const p2 = 'Juan Perez';

  const winner = thumbWar(p1, p2);
  expect(winner).toBe(p1);
  // Tu código acá:

  // cleanup
  // @ts-ignore
  utils.getWinner = originalGetWinner;
});

/**
 * Hints:
 * - https://jestjs.io/docs/en/mock-function-api#mockfnmockcalls
 * - https://jestjs.io/docs/en/expect#tohavebeencalledtimesnumber
 * - https://jestjs.io/docs/en/expect#tohavebeennthcalledwithnthcall-arg1-arg2-
 *
 * Checkout master branch to see the answer.
 */
