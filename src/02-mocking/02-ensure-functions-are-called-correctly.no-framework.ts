/**
 * Luego de haber usado las herramientas hechas por Jest, creemoslas nosotros
 * mismos.
 *
 * Tarea: Escribe una función 'fn' que crea un mock de la función y que
 * tiene un 'mock.calls'
 *
 * Ejecución: Use `ts-node src/02-mocking/02-ensure-functions-are-called-correctly.no-framework.ts` para ejecutar el test
 */

import assert from 'assert';
import { thumbWar } from './thumb-war';
import * as utils from './utils';

// Your Code:
const originalGetWinner = utils.getWinner;
// @ts-ignore
utils.getWinner = fn((p1, p2) => p1);

const winner = thumbWar('Luis Chodiman', 'Juan Perez');
assert.strictEqual(winner, 'Luis Chodiman');
// @ts-ignore
assert.deepStrictEqual(utils.getWinner.mock.calls, [
  ['Luis Chodiman', 'Juan Perez'],
  ['Luis Chodiman', 'Juan Perez'],
]);

// cleanup
// @ts-ignore
utils.getWinner = originalGetWinner;
