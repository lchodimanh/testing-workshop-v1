/**
 * Hasta ahora hemos hecho solo monkey patching, pero esto
 * nos traera problemas si queremos hacer un mock de un
 * ESModule, ya que no soporta este comportamiento por
 * la forma de exportar.
 * En vez de eso vamos a hacer mock del módulo completo
 * para que cuando el test pida el módulo encuentre una
 * versión completamente falsa.
 *
 * Tarea: refactoriza para hacer mock del módulo entero
 *
 * Execute: Use `jest --watch src/02-mocking/04-inline-module-mock.spec.ts`
 */

import { thumbWar } from './thumb-war';
import * as utils from './utils';

test('returns winner', () => {
  const originalGetWinner = utils.getWinner;
  // @ts-ignore
  utils.getWinner = jest.fn((p1, p2) => p1);

  const winner = thumbWar('Luis Chodiman', 'Juan Perez');
  expect(winner).toBe('Luis Chodiman');
  // @ts-ignore
  expect(utils.getWinner.mock.calls).toEqual([
    ['Luis Chodiman', 'Juan Perez'],
    ['Luis Chodiman', 'Juan Perez'],
  ]);

  // cleanup
  // @ts-ignore
  utils.getWinner = originalGetWinner;
});

/**
 * Pistas:
 * - https://jestjs.io/docs/en/es6-class-mocks#calling-jestmockdocsenjest-objectjestmockmodulename-factory-options-with-the-module-factory-parameter
 */
