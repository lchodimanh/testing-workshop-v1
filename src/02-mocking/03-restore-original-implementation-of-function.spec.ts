/**
 * Ahora vamos a usar una alternativa a monckey patching de tal
 * manera que no sea necesario tener la función original guardada
 * Tarea:
 *      1. usa spyOn de jest para hacer mock de la functión 'getWinner'
 *      2. restaura la versión original de 'getWinner' al finalizar
 *
 * Ejecuta: Use `npx jest --watch src/02-mocking/03-restore-original-implementation-of-function.spec.ts` para
 * correr el test en modo watch
 */

import { thumbWar } from './thumb-war';
import * as utils from './utils';

test('returns winner', () => {
  // Tu código:
  // Tip: si tienes errores de Typescript, ignóralos por ahora con //@ts-ignore

  const p1 = 'Luis Chodiman';
  const p2 = 'Juan Perez';
  const winner = thumbWar(p1, p2);

  expect(winner).toBe(p1);
  expect(utils.getWinner).toHaveBeenCalledTimes(2);

  // @ts-ignore
  expect(utils.getWinner.mock.calls).toEqual([
    [p1, p2],
    [p1, p2],
  ]);

  // Recuerda restaurar la versión original de 'getWinner'
  // @ts-ignore
});

// Tips: https://jestjs.io/docs/jest-object#jestspyonobject-methodname
