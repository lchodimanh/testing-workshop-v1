/**
 * Una desventaja es que si un test falla, los otros no se ejecutan ☹️
 * Creemos nuestra propia función para ejecutar tests y encapsularlos.
 * Además, aseguremonos de que todos los tests del archivo corran, incluso
 * si alguno falla.
 * Tarea: Encapsula los tests, de manera que podamos trabajar sobre los que
 *        fallaron
 * Ejecución: Usa `ts-node src/01-fundamentals/03-testing-framework.ts`.
 */

import { subtract, sum } from '../math';

test('sum adds numbers', () => {
  const result = sum(3, 7);
  const expected = 10;
  expect(result).toBe(expected);
});

test('subtract subtracts numbers', () => {
  const result = subtract(7, 3);
  const expected = 4;
  expect(result).toBe(expected);
});

function expect(actual: any) {
  return {
    toBe(expected: any) {
      if (actual !== expected) {
        throw new Error(`${actual} is not equal to ${expected}`);
      }
    },
    toEqual() {},
    toBeGreaterThan() {},
    // ...
  };
}

/**
 * Escribe el código acá
 */
