/**
 * Tenemos dos funciones, 'sum' y 'subtract'
 * que están listas para ser testeadas.
 *
 * Tarea: verifica que los resultados son los esperados
 *
 *            **BONUS**
 * Arroja un Error si el test falla
 *
 * Ejecución: Use `ts-node src/01-fundamentals/throw-an-error.ts` para
 * correr el test
 */

import { sum, subtract } from '../math';

/**
 * Pista: no hay magia, solo un condicional simple
 */
