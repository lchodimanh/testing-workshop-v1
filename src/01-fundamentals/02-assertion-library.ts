/**
 * Hemos probado "sum" y "subtract" en throw-an-error.ts,
 * pero este código es bastante imperativo.
 * Sería bueno escribir una pequeña abstracción para hacer
 * que se lea un poco mejor. Vamos a escribir una sencilla
 * abstracción para encapsular esta afirmación.
 *
 * Tarea:
 * 1. Cra una función llamada "expect" que acepte un "actual" (el valor real).
 * 2. Devuelve un objeto que tiene algunas afirmaciones. El primero
 * aquí va a ser "toBe", y eso va a tomar un valor esperado
 * ** BONIFICACIÓN **
 * ¿Puedes agregar más afirmaciones como ` toBeGreaterThan` y
 * `toBeLessThan`?
 *
 * Ejecutar: Usa `ts-node src/01-fundamentals/02-assertion-library.ts` para ejecutar la prueba.
 */
import { subtract, sum } from '../math';

let result, expected;

result = sum(3, 7);
expected = 10;

expect(result).toBe(expected);

result = subtract(7, 3);
expected = 4;

expect(result).toBe(expected);

// expect es una función que retorna un objeto que nos permite realizar aseveraciones
